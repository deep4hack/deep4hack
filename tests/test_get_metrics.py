import numpy as np
from src.metrics import get_metrics


def test_get_metrics():
    y_true = np.array(
        [
            [1, 1, 0, 0, 1],
            [1, 0, 1, 0, 1],
            [0, 1, 0, 1, 0],
            [0, 0, 1, 0, 1],
            [0, 0, 0, 1, 1],
            [1, 0, 1, 0, 1],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 1, 0],
        ]
    )

    y_pred = np.array(
        [
            [1, 0, 1, 1, 1],
            [1, 0, 1, 0, 1],
            [0, 1, 0, 0, 0],
            [0, 0, 1, 0, 1],
            [1, 1, 1, 0, 0],
            [0, 1, 0, 0, 1],
            [1, 0, 0, 0, 1],
            [0, 1, 1, 0, 0],
        ]
    )

    labels = ["Animals", "Bench", "Building", "Castle", "Cave"]

    m = get_metrics(y_true, y_pred, labels)
    # compare calculated f1 with the one calculated based on formula from github
    assert m["f1"] == (2 * 2 / 3 * 0.5 / (2 / 3 + 0.5) * 0.2) + (
        2 * 0.5 * 0.25 / (0.5 + 0.25) * 0.2
    ) + (2 * 2 / 3 * 0.4 / (2 / 3 + 0.4) * 0.2) + (
        2 * 0.8 * 0.8 / (0.8 + 0.8) * 0.2
    )
