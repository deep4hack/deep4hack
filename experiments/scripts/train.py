import argparse
import yaml


def get_args():
    """Parse cli arguments.

    input: path to data (preprocessed data)
    output: output path
    model: str, model name
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("input")
    parser.add_argument("output")
    parser.add_argument("model")
    parser.add_argument("--params", default="params.yaml")
    return parser.parse_args()


def load_config(config="params.yaml"):
    """Load config."""
    with open(config, "rt") as f:
        config = yaml.load(f, Loader=yaml.FullLoader)
    return config


def main():
    """Script entrypoint."""
    args = get_args()
    config = load_config()
    print(args, config)
    # models = {
    #     "default": None,
    # }

    # model = models[args.model](**yaml[model])
    # model.train()
    # model.dump()


if __name__ == "__main__":
    main()
