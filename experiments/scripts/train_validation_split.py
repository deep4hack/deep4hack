import argparse
import os

import numpy as np
import pandas as pd
from skmultilearn.model_selection import iterative_train_test_split


def get_args():
    """Parse cli arguments.

    input: path to data (preprocessed data)
    output: output path
    model: str, model name
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--input")
    parser.add_argument("--validation_ratio", default=0.25)
    parser.add_argument("--output")
    return parser.parse_args()


def main():
    """Script entrypoint."""
    args = get_args()
    os.makedirs(args.output, exist_ok=True)
    df = pd.read_csv(args.input)
    X = np.expand_dims(df["Name"].to_numpy(), axis=1)
    y = df[[c for c in df.columns if c != "Name"]].to_numpy()

    X_train, _, X_validation, _ = iterative_train_test_split(
        X, y, test_size=float(args.validation_ratio)
    )

    assert df.shape[0] == X_train.shape[0] + X_validation.shape[0]

    with open(os.path.join(args.output, "train.txt"), "w") as f:
        for line in X_train[:, 0].tolist():
            f.write(f"{line}\n")

    with open(os.path.join(args.output, "validation.txt"), "w") as f:
        for line in X_validation[:, 0].tolist():
            f.write(f"{line}\n")


if __name__ == "__main__":
    main()
