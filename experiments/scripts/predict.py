import argparse

from src.metrics import get_metrics, write_metrics_to_file  # noqa: F401


def get_args():
    """Parse cli arguments.

    input: path to data
    output: output path
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("input")
    parser.add_argument("output")
    return parser.parse_args()


def main():
    """Script entrypoint."""
    args = get_args()
    print(args)


if __name__ == "__main__":
    main()
