import json
import requests
import streamlit as st

from src.tools_manager import Manager
m = Manager(address=('audiotools', 50000), authkey=b'abc')
m.connect()
tools = m.ToolsProvider()


def audio_prediction(
    fd, window_len=10, topn=50, threshold=0.8, url="http://127.0.0.1:5000"
):
    pbar = st.progress(0)

    with st.spinner("Performing ASR..."):
        r = requests.post(url, files={"file": ("file.mp3", fd)})
        if r.status_code != 200:
            raise Exception("Server returned error: " + r.text)
        asr_file = json.loads(r.text)
        pbar.progress(10)

    with st.spinner('Generating synonyms...'):
        synonyms = tools.generate_synonyms(topn=topn, th=threshold)
        pbar.progress(20)

    results = []
    wg = list(tools.window_generator(asr_file, t=window_len))

    for i, row in enumerate(wg):
        row["tokens_lemmatized"] = tools.lemmatize(row["tokens"])
        row["labels"] = []
        for k, v in synonyms.items():
            if tools.have_common_tokens(v, row["tokens_lemmatized"]):
                row["labels"].append(k)
        results.append(row)
        pbar.progress(20 + (80 // len(wg)) * (i + 1))

    pbar.progress(100)
    return results


def audio_prediction2(
    fd,
    window_len=10,
    topn=50,
    threshold=0.75,
    magic_threshold=1.5,
    url="http://127.0.0.1:5000",
):
    pbar = st.progress(0)

    with st.spinner("Performing ASR..."):
        r = requests.post(url, files={"file": ("file.mp3", fd)})
        if r.status_code != 200:
            raise Exception("Server returned error: " + r.text)
        asr_file = json.loads(r.text)
        pbar.progress(10)

    with st.spinner('Generating synonyms...'):
        synonyms = tools.generate_synonyms(topn=topn, th=threshold)
        pbar.progress(20)

    results = []
    wg = list(tools.window_generator(asr_file, t=window_len))

    for i, row in enumerate(wg):
        row["tokens_lemmatized"] = tools.lemmatize(row["tokens"])
        row["labels"] = tools.get_closest_classes(
            row["tokens"],
            synonyms,
            magic_threshold)
        results.append(row)
        pbar.progress(20 + (80 // len(wg)) * (i + 1))

    pbar.progress(100)
    return results
