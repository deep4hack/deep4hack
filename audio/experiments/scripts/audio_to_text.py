import argparse
import os
import requests
import zipfile


def get_args():
    """Parse cli arguments.

    input: path to data
    output: output path
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--input")
    parser.add_argument("--output")
    return parser.parse_args()


def main():
    """Script entrypoint."""
    args = get_args()
    os.makedirs(args.output, exist_ok=True)

    with zipfile.ZipFile(args.input, "r") as zf:
        files_list = zf.namelist()
        for filename in files_list:
            if zf.getinfo(filename).is_dir():
                continue
            file_to_convert = zf.extract(filename, path="/tmp/")
            with open(file_to_convert, "rb") as f:
                filename = os.path.basename(filename)
                r = requests.post("http://127.0.0.1:5000", files={"file": f})
                if r.status_code != 200:
                    raise Exception("Server returned error: " + r.text)
                with open(f"{args.output}/{filename}.json", "wt") as f:
                    f.write(bytes(r.text, "utf8").decode("utf8"))
                os.remove(file_to_convert)


if __name__ == "__main__":
    main()
