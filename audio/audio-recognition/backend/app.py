import json
import os
import uuid

from flask import Flask
from flask import request
from pydub import AudioSegment

app = Flask(__name__)
app.config.from_object("config")
os.makedirs(app.config["UPLOAD_FOLDER"], exist_ok=True)


def allowed_file(filename):
    """Checks if file is in allowed format."""
    if "." in filename:
        if filename.rsplit(".", 1)[1].lower() in app.config["ALLOWED_FORMATS"]:
            return True
    return False


def data_to_json(data):
    """Converts time alligned result to json."""
    data = [d.strip() for d in data if not d.startswith("@")]
    data = [d.split(" ") for d in data if d]
    data = [
        {"start": float(d[2]), "duration": float(d[3]), "token": d[4]}
        for d in data
    ]
    return data


@app.route("/", methods=["POST"])
def acr():
    """Performs acr on MP3 file."""
    if "file" not in request.files:
        return "File is missing", 400
    app.logger.info("Receiving file")
    uploaded_file = request.files["file"]
    if uploaded_file and allowed_file(uploaded_file.filename):
        filename = str(uuid.uuid4())
        filename = os.path.join(app.config["UPLOAD_FOLDER"], filename)
        uploaded_file.save(filename)
        sound = AudioSegment.from_mp3(filename)
        sound.export(
            filename, format="wav", parameters=["-ac", "1", "-ar", "16000"]
        )

        app.logger.info("ACR")
        cmd = "/tools/Recognize/run.sh {} {}".format(
            filename, filename + ".txt"
        )
        os.system(cmd)

        cmd = "/tools/ForcedAlign/run.sh {} {} {}".format(
            filename, filename + ".txt", filename + ".ctm"
        )
        os.system(cmd)

        with open(filename + ".ctm", encoding="utf8") as f:
            data = f.readlines()
        data = data_to_json(data)

        os.remove(filename)
        os.remove(filename + ".txt")
        os.remove(filename + ".ctm")
        return json.dumps(data), 200
    return "The file is corrupted or is not in allowed format", 400
