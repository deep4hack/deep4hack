#!/usr/bin/env bash

if [[ "${#}" == 1 ]]; then
	TAG="${1}"
else
	TAG="latest"
fi

docker run -it \
       -v "${PWD}:/home/deep4hack" \
       --gpus all \
       --name deep4hack-"${TAG}" \
       deep4hack:"${TAG}"
