import argparse
import os
import zipfile


def get_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser()
    parser.add_argument("--input")
    parser.add_argument("--output")
    parser.add_argument("--members", default=None)
    return parser.parse_args()


def main():
    """Script entrypoint."""
    args = get_args()
    os.makedirs(args.output, exist_ok=True)
    if args.members:
        if isinstance(args.members, str):
            members = [args.members]
        elif isinstance(args.members, list):
            members = args.members
        else:
            raise ValueError(
                f"Wrong type of members! Supported str or list. "
                f"Is {type(args.members)}"
            )
    else:
        members = None
    with zipfile.ZipFile(args.input, "r") as f:
        f.extractall(args.output, members=members)


if __name__ == "__main__":
    main()
