#!/usr/bin/env bash

if [[ "${#}" == 1 ]]; then
	TAG="${1}"
else
	TAG="latest"
fi

docker build --file Dockerfile \
             --tag deep4hack:"${TAG}" \
             "$PWD"
