FROM python:3.8

RUN pip install \
    numpy \
    spacy \
    scipy \
    gensim

RUN python3.8 -m spacy download pl_core_news_sm

WORKDIR /home/manager
COPY translations.json .
COPY src/tools_manager.py .
# COPY data/audio/models/cbow_v300m8_hs.w2v.txt . mount it!

ENTRYPOINT ["python", "tools_manager.py"]
    
