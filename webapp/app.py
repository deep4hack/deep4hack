"""Streamlit application."""
from collections import Counter
import io
from itertools import chain
import os
import tempfile

import numpy as np
import pandas as pd
import plotly.express as px
import pydub
import streamlit as st
import torch

from audio.audio import (
   audio_prediction as audio_prediction_intersection,
   audio_prediction2 as audio_prediction_embedding,
)
from src.video import frame_extractor
from src.common import Config
from src.seresnext.model import Model as ImageModel

FILE_TYPES = ("Video", "Audio")
FILE_EXTENSIONS = {
    "Video": ["mp4"],
    "Audio": ["mp3"],
}

LABEL_NAMES = [
    "Amusement park",
    "Animals",
    "Bench",
    "Building",
    "Castle",
    "Cave",
    "Church",
    "City",
    "Cross",
    "Cultural institution",
    "Food",
    "Footpath",
    "Forest",
    "Furniture",
    "Grass",
    "Graveyard",
    "Lake",
    "Landscape",
    "Mine",
    "Monument",
    "Motor vehicle",
    "Mountains",
    "Museum",
    "Open-air museum",
    "Park",
    "Person",
    "Plants",
    "Reservoir",
    "River",
    "Road",
    "Rocks",
    "Snow",
    "Sport",
    "Sports facility",
    "Stairs",
    "Trees",
    "Watercraft",
    "Windows",
]

LABEL_THRESHOLDS = [
    0.1020,
    0.2041,
    0.2245,
    0.2449,
    0.0816,
    0.5000,
    0.3469,
    0.4286,
    0.1837,
    0.2449,
    0.1837,
    0.3469,
    0.2653,
    0.2449,
    0.2857,
    0.1429,
    0.1837,
    0.3265,
    0.2245,
    0.2449,
    0.5000,
    0.5714,
    0.2041,
    0.5000,
    0.6327,
    0.4694,
    0.3265,
    0.1429,
    0.2653,
    0.3878,
    0.2245,
    0.1020,
    0.4490,
    0.3265,
    0.5000,
    0.3469,
    0.3061,
    0.2653,
]


# def _process_image(raw_data):
#     image = Image.open(fp=raw_data)
#
#     st.markdown("## Uploaded image")
#     st.image(image=image, caption=raw_data.name, use_column_width=True)
#
#     x = np.array(image).reshape(1, -1)
#     labels = _apply_model(x)[0]
#
#     st.markdown("## Classification results")
#     df = pd.DataFrame.from_records(
#         zip(LABEL_NAMES, labels), columns=("name", "value")
#     ).set_index("name")
#
#     st.table(df)


@st.cache
def _load_model():
    config = Config["cnn_image"]
    model = ImageModel(
        model_name=config["architecture"]["model_name"],
        num_labels=config["architecture"]["num_labels"],
    )
    model.load_state_dict(torch.load("models/seresnext/model.pt"))
    model.eval()
    return model


def _apply_model(model, x: np.ndarray, times):
    config = Config["cnn_image"]
    with st.spinner('Applying model'):
        _, preds = model.infer_on_sample(
            x=x, max_input_size=config["architecture"]["image_size"]
        )

    video_res = [
        {
            "timestamp": int(t / 1000),
            "labels": [
                ln
                for ln, p, th in zip(LABEL_NAMES, ps, LABEL_THRESHOLDS)
                if p >= th
            ],
        }
        for t, ps in zip(times, preds.tolist())
    ]

    return video_res


def _process_video(raw_data):
    st.markdown("## Uploaded video")
    st.video(data=raw_data)

    _, tf_path = tempfile.mkstemp(suffix=".mp4", dir="./")
    with open(tf_path, "wb") as fout:
        fout.write(raw_data.read())

    interval = st.number_input(
        label="Frame interval [seconds]", min_value=1, step=1, value=1
    )
    extracted = list(
        frame_extractor(filename=tf_path, interval_ms=1_000 * interval)
    )
    times = [i for i, _ in extracted]
    frames = np.stack([f for _, f in extracted])

    model = _load_model()
    if st.button(label='Run model'):
        video_res = _apply_model(model, frames, times)

        with st.beta_expander("Model predictions", expanded=True):
            st.table(video_res)

        with st.beta_expander("Statistics", expanded=True):
            _video_full_histogram(video_res)
            _video_single_label_count_timeline(video_res)
            _video_all_label_count_timeline(video_res)
    os.unlink(tf_path)


def _video_full_histogram(video_res):
    counted_labels = Counter(chain(*[v["labels"] for v in video_res]))
    total = sum(counted_labels.values(), 0.0)
    counted_labels = [
        {"name": ln, "value": counted_labels[ln] / total * 100.0}
        for ln in LABEL_NAMES
    ]

    df = pd.DataFrame.from_records(counted_labels)
    fig = px.bar(
        data_frame=df,
        x="name",
        y="value",
        title="Histogram of label occurrence",
        labels={"name": "Class name", "value": "%"},
    )
    fig.update_xaxes(dtick=1)
    st.plotly_chart(fig)


def _video_single_label_count_timeline(video_res):
    df = pd.DataFrame.from_records(
        [
            {
                "timestamp": vr["timestamp"],
                "label": label,
                "count": 1 if label in vr["labels"] else 0,
            }
            for vr in video_res
            for label in LABEL_NAMES
        ]
    )
    fig = px.line(
        data_frame=df,
        x="timestamp",
        y="count",
        color="label",
        labels={
            "timestamp": "Frame timestamp",
            "count": "Numer of predictions",
            "label": "Class name",
        },
    )
    fig.update_xaxes(dtick=1)
    st.plotly_chart(fig)


def _video_all_label_count_timeline(video_res):
    df = pd.DataFrame.from_records(
        [
            {
                "timestamp": vr["timestamp"],
                "count": len(vr["labels"]),
            }
        for vr in video_res
        ]
    )
    fig = px.line(
        data_frame=df,
        # x='time_range',
        x="timestamp",
        y="count",
        title="Number of predicted labels in frame",
        labels={
            #'time_range': 'Time window',
            "timestamp": "Frame timestamp",
            "count": "Number of predicted labels",
        },
    )
    fig.update_xaxes(dtick=1)
    st.plotly_chart(fig)


def _trim_audio(raw_data, interval_ms):
    audio = pydub.AudioSegment.from_file(file=raw_data)
    audio = audio[:interval_ms]

    buf = io.BytesIO()
    audio.export(out_f=buf)
    buf.seek(0)
    return buf


def _audio_full_histogram(audio_res):
    counted_labels = Counter(chain(*[v["labels"] for v in audio_res]))
    total = sum(counted_labels.values(), 0.0)
    counted_labels = [
        {"name": ln, "value": counted_labels[ln] / total * 100.0}
        for ln in LABEL_NAMES
    ]

    df = pd.DataFrame.from_records(counted_labels)
    fig = px.bar(
        data_frame=df,
        x="name",
        y="value",
        title="Histogram of label occurrence",
        labels={"name": "Class name", "value": "%"},
    )
    fig.update_xaxes(dtick=1)
    st.plotly_chart(fig)


def _audio_single_label_count_timeline(audio_res):
    df = pd.DataFrame.from_records(
        [
            {
                "time_range": ar["time_range"],
                "start": ar["start"],
                "end": ar["end"],
                "label": label,
                "count": 1 if label in ar["labels"] else 0,
            }
            for ar in audio_res
            for label in LABEL_NAMES
        ]
    )
    fig = px.line(
        data_frame=df,
        x="end",
        y="count",
        color="label",
        labels={
            "end": "End time of window",
            "count": "Numer of predictions",
            "label": "Class name",
        },
    )
    fig.update_xaxes(dtick=1)
    st.plotly_chart(fig)


def _audio_all_label_count_timeline(audio_res):
    df = pd.DataFrame.from_records(
        [
            {
                "time_range": ar["time_range"],
                "start": ar["start"],
                "end": ar["end"],
                "count": len(ar["labels"]),
            }
            for ar in audio_res
        ]
    )
    fig = px.line(
        data_frame=df,
        # x='time_range',
        x="end",
        y="count",
        title="Number of predicted labels in time window",
        labels={
            #'time_range': 'Time window',
            "end": "End time of window",
            "count": "Numer of predicted labels",
        },
    )
    fig.update_xaxes(dtick=1)
    st.plotly_chart(fig)


def _process_audio(raw_data):
    MAX_DURATION_MS = 180_000
    raw_data = _trim_audio(raw_data=raw_data, interval_ms=MAX_DURATION_MS)

    st.markdown("## Uploaded audio file")
    st.audio(data=raw_data)

    with st.beta_expander("Algorithm settings"):
        alg_type = st.radio(
            label="Choose algorithm version",
            options=["Similarity based", "Embedding based"],
            index=1,
        )

        window_len_seconds = st.number_input(
            label="Window size [seconds]", min_value=5, step=5, value=10
        )

        topn = st.number_input(
            label="Number of synonyms",
            min_value=10,
            max_value=200,
            step=10,
            value=50,
        )

        threshold = st.number_input(
            label="Similarity threshold",
            min_value=0.5,
            max_value=1.0,
            value=0.75,
            step=0.05,
        )

        params = dict(
            url="http://audio-recognition:5000/",
            window_len=window_len_seconds,
            topn=topn,
            threshold=threshold,
        )

        if alg_type == "Embedding based":
            magic_threshold = st.number_input(
                label="Sigma threshold",
                min_value=1.0,
                max_value=2.0,
                value=1.5,
                step=0.1,
            )
            params["magic_threshold"] = magic_threshold

    if st.button("Run model"):
        raw_data.seek(0)

        if alg_type == "Similarity based":
            audio_res = audio_prediction_intersection(fd=raw_data, **params)
        elif alg_type == "Embedding based":
            audio_res = audio_prediction_embedding(fd=raw_data, **params)
        else:
            raise RuntimeError("Unknown algorithm")

        audio_res = [
            {
                "time_range": str(v["start"]) + " - " + str(v["end"]),
                "start": v["start"],
                "end": v["end"],
                "tokens": v["tokens"],
                "token_lemmatized": v["tokens_lemmatized"],
                "labels": v["labels"],
            }
            for v in audio_res
        ]

        with st.beta_expander("Model predictions", expanded=True):
            st.table(
                [
                    {
                        "time_range": ar["time_range"],
                        "text": " ".join(ar["tokens"]),
                        "labels": ar["labels"],
                    }
                    for ar in audio_res
                ]
            )

        with st.beta_expander("Statistics", expanded=True):
            _audio_full_histogram(audio_res)
            _audio_single_label_count_timeline(audio_res)
            _audio_all_label_count_timeline(audio_res)


def _write_model_description(file_type: str):
    with st.beta_expander(f'How does the "{file_type}" model work?'):
        st.markdown(f"**TODO {file_type}**")


def main():
    _processing_fns = {
        # "Image": _process_image,
        "Video": _process_video,
        "Audio": _process_audio,
    }

    st.title("[Deep4Hack] Silesian object recognition app")
    st.markdown("### Choose what you want to upload")
    ftype = st.radio(label="", options=FILE_TYPES)

    # _write_model_description(file_type=ftype)

    fu = st.file_uploader(
        label="Choose a file to upload",
        type=FILE_EXTENSIONS[ftype],
        accept_multiple_files=False,
    )
    if fu:
        _processing_fns[ftype](fu)


if __name__ == "__main__":
    main()
