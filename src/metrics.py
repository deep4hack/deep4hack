import json
from typing import List

from sklearn.metrics import (
    f1_score,
    accuracy_score,
    precision_score,
    recall_score,
    roc_auc_score,
)
import numpy as np


def get_metrics(
    y_true: np.ndarray,
    y_pred: np.ndarray,
    labels: List[str],
    y_pred_logits: np.ndarray = None,
) -> dict:
    m = {
        "f1": f1_score(y_true, y_pred, average="macro"),
        "precision": precision_score(y_true, y_pred, average="macro"),
        "recall": recall_score(y_true, y_pred, average="macro"),
        "accuracy": accuracy_score(y_true, y_pred),
    }
    if y_pred_logits is not None:
        m["auc"] = roc_auc_score(y_true, y_pred_logits, average="macro")
    m.update(
        {
            "labels-f1": dict(),
            "labels-precision": dict(),
            "labels-recall": dict(),
        }
    )
    for idx, l in enumerate(labels):
        m["labels-f1"][l] = f1_score(y_true[:, idx], y_pred[:, idx])
        m["labels-precision"][l] = precision_score(
            y_true[:, idx], y_pred[:, idx]
        )
        m["labels-recall"][l] = recall_score(y_true[:, idx], y_pred[:, idx])
    return m


def write_metrics_to_file(metrics: dict, path: str):
    with open(path, "w") as f:
        json.dump(metrics, f, indent=4)
