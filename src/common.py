import numpy as np
import torch
import yaml

with open("params.yaml") as f:
    Config = yaml.safe_load(f)


def copy2cpu(tensor: torch.Tensor) -> np.ndarray:
    return tensor.detach().cpu().numpy()
