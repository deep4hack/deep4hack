import json

from multiprocessing.managers import BaseManager
import numpy as np
import spacy
import scipy as sc
from gensim.models import KeyedVectors


# spacy.cli.download('pl_core_news_sm')

model = None
nlp = None
ALL_SPEECH_PARTS = [
    'noun', 'adj', 'verb', 'num', 'adv', 'pron', 'prep',
    'conj', 'interj', 'burk', 'qub', 'xxx', 'interp', 'aglt'
]


def load_json(filename):
    with open(filename, "rt") as f:
        data = json.load(f)
    return data


class ToolsProvider(object):

    def __init__(self):
        pass

    def get_model(self):
        return nlp

    def window_generator(self, filename_or_dict, t):
        """Yields list of tokens using time window"""

        if isinstance(filename_or_dict, str):
            with open(filename_or_dict) as f:
                data = json.load(f)
        else:
            data = filename_or_dict
        result = []
        
        start = data[0]["start"]
        tokens = [data[0]["token"]]
        for row in data[1:]:
            if start + t > row["start"]:
                tokens.append(row["token"])
                end = row["start"]
            else:
                result.append({
                    "tokens": tokens,
                    "start": start,
                    "end": end
                })
                start = row["start"]
                tokens = [row["token"]]
        result.append({
            "tokens": tokens,
            "start": start,
            "end": end
        })
        return result


    def lemmatize(self, tokens, selection_list=["VERB", "NOUN", "ADJ"]):
        """Lematize tokens."""
        if isinstance(tokens, list):
            text = " ".join(tokens)
        else:
            text = tokens
        doc = nlp(text)
        if selection_list:
            doc = [token for token in doc if token.pos_ in selection_list]
        return [token.lemma_ for token in doc]

        
    def generate_synonyms(self, filename="./translations.json", topn=20, th=0.8):
        data = load_json(filename)
        synonyms = dict()
        for k, v in data.items():
            synonyms_for_k = v["translation"][:]
            tokens = self.lemmatize(v["translation"])
            for token in tokens:
                synonyms_for_k.append(token)
                poss_words = [f"{token}::{pos}" for pos in ALL_SPEECH_PARTS]
                for pw in  poss_words:
                    if pw in model:
                        synonyms_for_k += [
                            k[0].split("::")[0]
                            for k in
                            model.most_similar(positive=[pw], topn=topn)
                            if float(k[1]) > th
                        ]
            synonyms[k] = list(set(synonyms_for_k))
        return synonyms


    def mean_vector_from_tokens(self, tokens):
        vectors = []
        for token in tokens:
            poss_words = [f"{token}::{pos}" for pos in ALL_SPEECH_PARTS]
            for pw in  poss_words:
                if pw in model:
                    vectors.append(model[pw])
        if not vectors:
            print(f"No tokens {tokens}")
            vectors = [[0.0] * 300]
        vectors = np.stack(vectors)
        return vectors.mean(axis=0)


    def get_closest_classes(self, list1, synonyms, magic=1.0):

        synonym_list = []
        synonym_vectors = []
        result = []
        for k, v in synonyms.items():
            synonym_list.append(k)
            synonym_vectors.append(self.mean_vector_from_tokens(v))
        
        distance_matrix = sc.spatial.distance.cdist(
            np.stack(synonym_vectors),
            np.stack(synonym_vectors),
            metric="euclidean"
        )
        std_distance = np.std(distance_matrix.flatten())
        mean_distance = distance_matrix.flatten().mean()
        lemm_tokens = self.lemmatize(list1, selection_list=["NOUN"])
        if not lemm_tokens:
            return []
        mean_token = self.mean_vector_from_tokens(lemm_tokens)
        for i, vec in enumerate(synonym_vectors):
            dist = np.linalg.norm(vec - mean_token)
            if dist < mean_distance - std_distance * magic:
                result.append(synonym_list[i])
        return result


    def have_common_tokens(self, list1, list2):
        for x in list1:
            for y in list2:
                if y == x:
                    return True
        return False


class Manager(BaseManager):
    pass

Manager.register(
    "ToolsProvider",
    ToolsProvider,
    exposed=[
        "have_common_tokens",
        "get_closest_classes",
        "get_model",
        "generate_synonyms",
        "window_generator",
        "lemmatize"
    ]
)

if __name__ == "__main__":
    print("Loading models")
    nlp = spacy.load("pl_core_news_sm")
    model = KeyedVectors.load_word2vec_format(
        "./cbow_v300m8_hs.w2v.txt",
        binary=False
    )
    print("Loaded!")
    manager = Manager(address=('', 50000), authkey=b'abc')
    server = manager.get_server()
    server.serve_forever()
