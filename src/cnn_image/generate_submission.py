from pathlib import Path
import numpy as np

import click
import pandas as pd
import torch
import tqdm
from sklearn.metrics import f1_score
from src.cnn_image.data import get_dataloaders, get_test_loader
from src.cnn_image.model import Model
from src.common import Config, copy2cpu
from src.metrics import get_metrics, write_metrics_to_file

use_cuda = torch.cuda.is_available()


@click.command()
@click.argument("data_dir")
@click.argument("test_data_dir")
@click.argument("training_labels_file")
@click.argument("splits_dir")
@click.argument("model_dir")
@click.argument("output_dir")
@torch.no_grad()
def generate_submission(
    data_dir: str,
    test_data_dir: str,
    training_labels_file: str,
    splits_dir: str,
    model_dir: str,
    output_dir: str,
):
    config = Config["cnn_image"]
    test_config = config["test"]

    output_dir_path = Path(output_dir)
    output_dir_path.mkdir(exist_ok=True, parents=True)

    _, valid_dataloader = get_dataloaders(
        data_dir,
        training_labels_file,
        splits_dir,
        batch_size=test_config["batch_size"] + 1,
        image_size=config["architecture"]["image_size"],
        num_workers=test_config["batch_size"],
        test_time_augmentation=test_config["test_time_augmentation"],
        num_test_time_augmentations=test_config["num_test_time_augmentations"],
    )
    test_loader = get_test_loader(
        test_data_dir,
        test_time_augmentation=test_config["test_time_augmentation"],
        num_test_time_augmentations=test_config["num_test_time_augmentations"],
        image_size=config["architecture"]["image_size"],
        num_workers=test_config["batch_size"] + 1,
        batch_size=test_config["batch_size"],
    )

    model = Model(
        config["architecture"]["model_name"],
        config["architecture"]["num_labels"],
    )
    model.load_state_dict(torch.load(Path(model_dir) / "model.pt"))
    if use_cuda:
        model = model.cuda()
    model.eval()

    all_predictions = []
    all_ground_truths = []
    for batch in tqdm.tqdm(valid_dataloader):
        images = batch["images"]
        labels = batch["labels"]

        if use_cuda:
            images = images.cuda()
            labels = labels.cuda()

        for sample_set in images:
            predictions = model(sample_set).mean(dim=0)
            all_predictions.append(predictions.sigmoid())
        all_ground_truths.append(labels.float())

    all_predictions_tensor = torch.stack(all_predictions, dim=0)
    all_ground_truths_tensor = torch.cat(all_ground_truths, dim=0)

    best_score = 0.0
    best_tresholdings = (
        torch.tensor([0.5] * config["architecture"]["num_labels"])
        .to(all_predictions_tensor)
        .unsqueeze(dim=0)
    )

    for feature_index in range(config["architecture"]["num_labels"]):
        for threshold in np.linspace(0.0, 1.0):
            copy_threshold = best_tresholdings.clone()
            copy_threshold[:, feature_index] = threshold
            score = f1_score(
                copy2cpu(all_ground_truths_tensor.view((-1,)).byte()),
                copy2cpu(
                    (all_predictions_tensor >= copy_threshold)
                    .view((-1,))
                    .byte()
                ),
                average="macro",
            )
            if score > best_score:
                best_score = score
                best_tresholdings = copy_threshold

    print(best_tresholdings)
    print(best_score)
    columns = pd.read_csv(training_labels_file).columns
    labels = columns[1:]
    metrics = get_metrics(
        copy2cpu(all_ground_truths_tensor),
        copy2cpu((all_predictions_tensor >= best_tresholdings).byte()),
        labels=labels,
        y_pred_logits=copy2cpu(all_predictions_tensor),
    )

    write_metrics_to_file(metrics, output_dir_path / "metrics.json")
    submission_data = []

    for batch in tqdm.tqdm(test_loader):
        images = batch["images"]
        names = batch["names"]

        if use_cuda:
            images = images.cuda()

        for name, sample_set in zip(names, images):
            predictions = (
                model(sample_set).mean(dim=0).sigmoid() >= best_tresholdings[0]
            ).byte()
            submission_data.append([name] + list(copy2cpu(predictions)))

    pd.DataFrame(submission_data, columns=columns).sort_values(
        by="Name"
    ).to_csv(
        output_dir_path / "submission_{:.6f}".format(best_score), index=False
    )


if __name__ == "__main__":
    generate_submission()
