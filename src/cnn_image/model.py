from typing import List, Tuple, Union

import numpy as np
import pretrainedmodels
import torch
import torch.nn as nn
from src.cnn_image.data import get_test_transforms
from src.common import copy2cpu


class Model(nn.Module):
    def __init__(self, model_name: str, num_labels: int) -> None:
        super().__init__()

        model = pretrainedmodels.inceptionv4()
        self.features = model.features
        self.register_buffer(
            "mean", torch.tensor(model.mean)[None, :, None, None]
        )
        self.register_buffer(
            "std", torch.tensor(model.std)[None, :, None, None]
        )
        for param in self.features.parameters():
            param.requires_grad_ = False
        self.classifier = nn.Sequential(
            nn.Dropout(0.5), nn.ELU(), nn.Linear(1536 * 8 * 8, num_labels)
        )

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = (x - self.mean) / self.std
        features = self.features(x).view((x.size(0), -1))
        return self.classifier(features)

    def get_features(self, x: torch.Tensor) -> torch.Tensor:
        x = (x - self.mean) / self.std
        features = self.features(x).view((x.size(0), -1))
        return features

    @property
    def device(self) -> torch.device:
        return next(self.parameters()).device

    @torch.no_grad()
    def infer_on_sample(
        self, x: Union[List[np.ndarray], np.ndarray], max_input_size: int
    ) -> Tuple[np.ndarray, np.ndarray]:
        logits_list, features_list = [], []
        for sample in x:
            batch_list = []
            transforms = get_test_transforms(
                sample.shape[0], sample.shape[1], max_input_size
            )
            image_tensor = transforms(sample)
            batch_list.append(image_tensor)
            batch = torch.stack(batch_list, dim=0).to(self.device)
            batch = (batch - self.mean) / self.std
            features = self.features(batch).mean(dim=(2, 3))
            logits = self.classifier(features).sigmoid()

            logits_list.append(copy2cpu(logits.mean(dim=0)))
            features_list.append(copy2cpu(features.mean(dim=0)))

        return np.stack(features_list, axis=0), np.stack(logits_list, axis=0)
