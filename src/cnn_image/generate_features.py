import multiprocessing as mp
import pickle
from collections import defaultdict
from pathlib import Path
from typing import Any, Dict

import click
import torch
import torch.nn as nn
import tqdm
from src.cnn_image.data import get_dataloaders, get_test_loader
from src.cnn_image.model import Model
from src.common import Config, copy2cpu
from torch.utils.data.dataloader import DataLoader

use_cuda = torch.cuda.is_available()


def get_loader_for_a_single_loader(
    loader: DataLoader, model: nn.Module
) -> Dict[str, Any]:
    sample_data: Dict[str, Any] = defaultdict(list)

    for batch in tqdm.tqdm(loader):
        images = batch["images"]
        labels = batch["labels"]
        names = batch["names"]

        if use_cuda:
            images = images.cuda()
            labels = labels.cuda()

        for name, sample_set, labeling in zip(names, images, labels):
            features = model.get_features(sample_set).mean(dim=0)
            predictions_without_sigmoid = model(sample_set).mean(dim=0)
            sample_data["features"].append(features)
            sample_data["predictions_without_sigmoid"].append(
                predictions_without_sigmoid
            )
            sample_data["name"].append(name)
            sample_data["labels"].append(labeling)

    return {
        "features": copy2cpu(torch.stack(sample_data["features"], dim=0)),
        "predictions_without_sigmoid": copy2cpu(
            torch.stack(sample_data["predictions_without_sigmoid"], dim=0)
        ),
        "name": sample_data["name"],
        "labels": copy2cpu(torch.stack(sample_data["labels"], dim=0)),
    }


def get_loader_for_a_test_loader(
    loader: DataLoader, model: nn.Module
) -> Dict[str, Any]:
    sample_data: Dict[str, Any] = defaultdict(list)

    for batch in tqdm.tqdm(loader):
        images = batch["images"]
        names = batch["names"]

        if use_cuda:
            images = images.cuda()

        for name, sample_set in zip(names, images):
            features = model.get_features(sample_set).mean(dim=0)
            predictions_without_sigmoid = model(sample_set).mean(dim=0)
            sample_data["features"].append(features)
            sample_data["predictions_without_sigmoid"].append(
                predictions_without_sigmoid
            )
            sample_data["name"].append(name)

    return {
        "features": copy2cpu(torch.stack(sample_data["features"], dim=0)),
        "predictions_without_sigmoid": copy2cpu(
            torch.stack(sample_data["predictions_without_sigmoid"], dim=0)
        ),
        "name": sample_data["name"],
    }


@click.command()
@click.argument("data_dir")
@click.argument("test_data_dir")
@click.argument("training_labels_file")
@click.argument("splits_dir")
@click.argument("model_dir")
@click.argument("output_dir")
@torch.no_grad()
def generate_features(
    data_dir: str,
    test_data_dir: str,
    training_labels_file: str,
    splits_dir: str,
    model_dir: str,
    output_dir: str,
):
    config = Config["cnn_image"]
    test_config = config["test"]

    output_dir_path = Path(output_dir)
    output_dir_path.mkdir(exist_ok=True, parents=True)

    train_dataloader, valid_dataloader = get_dataloaders(
        data_dir,
        training_labels_file,
        splits_dir,
        batch_size=test_config["batch_size"],
        image_size=config["architecture"]["image_size"],
        num_workers=mp.cpu_count() - 1,
        test_time_augmentation=test_config["test_time_augmentation"],
        num_test_time_augmentations=test_config["num_test_time_augmentations"],
        no_shuffling=True,
    )
    test_loader = get_test_loader(
        test_data_dir,
        test_config["test_time_augmentation"],
        test_config["num_test_time_augmentations"],
        image_size=config["architecture"]["image_size"],
        num_workers=mp.cpu_count() - 1,
        batch_size=test_config["batch_size"],
    )

    model = Model(
        config["architecture"]["model_name"],
        config["architecture"]["num_labels"],
    )
    model.load_state_dict(torch.load(Path(model_dir) / "model.pt"))
    if use_cuda:
        model = model.cuda()
    model.eval()

    with open(output_dir_path / "train.pkl", "wb") as f:
        pickle.dump(get_loader_for_a_single_loader(train_dataloader, model), f)
    with open(output_dir_path / "valid.pkl", "wb") as f:
        pickle.dump(get_loader_for_a_single_loader(valid_dataloader, model), f)
    with open(output_dir_path / "test.pkl", "wb") as f:
        pickle.dump(get_loader_for_a_test_loader(test_loader, model), f)


if __name__ == "__main__":
    generate_features()
