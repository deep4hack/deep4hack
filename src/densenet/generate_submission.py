from pathlib import Path

import click
import pandas as pd
import torch
import tqdm
from sklearn.metrics import f1_score
from src.densenet.data import get_dataloaders, get_test_loader
from src.densenet.model import Model
from src.common import Config, copy2cpu
from src.metrics import get_metrics, write_metrics_to_file

use_cuda = torch.cuda.is_available()


@click.command()
@click.argument("data_dir")
@click.argument("test_data_dir")
@click.argument("training_labels_file")
@click.argument("splits_dir")
@click.argument("model_dir")
@click.argument("output_dir")
@torch.no_grad()
def generate_submission(
    data_dir: str,
    test_data_dir: str,
    training_labels_file: str,
    splits_dir: str,
    model_dir: str,
    output_dir: str,
):
    config = Config["cnn_image"]
    test_config = config["test"]

    output_dir_path = Path(output_dir)
    output_dir_path.mkdir(exist_ok=True, parents=True)

    _, valid_dataloader = get_dataloaders(
        data_dir,
        training_labels_file,
        splits_dir,
        batch_size=test_config["batch_size"] + 1,
        image_size=config["architecture"]["image_size"],
        num_workers=test_config["batch_size"],
    )
    test_loader = get_test_loader(
        test_data_dir,
        test_config["test_time_augmentation"],
        test_config["num_test_time_augmentations"],
        image_size=config["architecture"]["image_size"],
        num_workers=test_config["batch_size"] + 1,
        batch_size=test_config["batch_size"],
    )

    model = Model(
        config["architecture"]["model_name"],
        config["architecture"]["num_labels"],
    )
    model.load_state_dict(torch.load(Path(model_dir) / "model.pt"))
    if use_cuda:
        model = model.cuda()
    model.eval()

    all_predictions = []
    all_ground_truths = []
    for batch in tqdm.tqdm(valid_dataloader):
        images = batch["images"]
        labels = batch["labels"]

        if use_cuda:
            images = images.cuda()
            labels = labels.cuda()

        predictions = model(images)
        all_ground_truths.append(labels.float())
        all_predictions.append(predictions.sigmoid())

    all_predictions_tensor = torch.cat(all_predictions, dim=0)
    all_ground_truths_tensor = torch.cat(all_ground_truths, dim=0)

    score = f1_score(
        copy2cpu(all_ground_truths_tensor.view((-1,)).byte()),
        copy2cpu(
            (
                all_predictions_tensor
                >= config["architecture"]["prediction_threshold"]
            )
            .view((-1,))
            .byte()
        ),
    )

    columns = pd.read_csv(training_labels_file).columns
    labels = columns[1:]
    metrics = get_metrics(
        copy2cpu(all_ground_truths_tensor),
        copy2cpu(
            (
                all_predictions_tensor
                >= config["architecture"]["prediction_threshold"]
            ).byte()
        ),
        labels=labels,
        y_pred_logits=copy2cpu(all_predictions_tensor.sigmoid()),
    )

    write_metrics_to_file(metrics, output_dir_path / "metrics.json")
    submission_data = []

    for batch in tqdm.tqdm(test_loader):
        images = batch["images"]
        names = batch["names"]

        if use_cuda:
            images = images.cuda()

        for name, sample_set in zip(names, images):
            predictions = (
                model(sample_set).mean(dim=0).sigmoid()
                >= config["architecture"]["prediction_threshold"]
            ).byte()
            submission_data.append([name] + list(copy2cpu(predictions)))

    pd.DataFrame(submission_data, columns=columns).sort_values(
        by="Name"
    ).to_csv(output_dir_path / "submission_{:.6f}".format(score), index=False)


if __name__ == "__main__":
    generate_submission()
