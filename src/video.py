import cv2
import numpy as np


def frame_extractor(filename: str, interval_ms: int):
    cap = cv2.VideoCapture(filename)
    interval_frames = np.round(
        cap.get(cv2.CAP_PROP_FPS) * (interval_ms / 1_000)
    )

    while True:
        ret, frame = cap.read()

        curr_id = cap.get(cv2.CAP_PROP_POS_FRAMES)
        curr_time = cap.get(cv2.CAP_PROP_POS_MSEC)

        if not ret:
            break

        if (curr_id - 1) % interval_frames == 0:
            yield curr_time, frame


def main():
    fname = "data/sample/videos/film_dron_2.mp4"

    for time, frame in frame_extractor(filename=fname, interval_ms=1_000):
        print(time, frame.shape)
    breakpoint()


if __name__ == "__main__":
    main()
