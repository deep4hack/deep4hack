from typing import List, Tuple, Union

import numpy as np
import pretrainedmodels
import torch
import torch.nn as nn
from src.common import copy2cpu
from src.seresnext.data import get_test_transforms


class Model(nn.Module):
    def __init__(self, model_name: str, num_labels: int) -> None:
        super().__init__()

        self.model = pretrainedmodels.se_resnext50_32x4d()
        self.features = self.model.features
        self.register_buffer(
            "mean", torch.tensor(self.model.mean)[None, :, None, None]
        )
        self.register_buffer(
            "std", torch.tensor(self.model.std)[None, :, None, None]
        )
        self.model.layer0.requires_grad_ = False
        self.model.layer1.requires_grad_ = False
        self.model.layer2.requires_grad_ = False
        self.model.layer3.requires_grad_ = False
        self.model.layer4.requires_grad_ = False

        self.classifier = nn.Sequential(
            nn.Linear(2048, 4096),
            nn.ELU(inplace=True),
            nn.Dropout(0.5),
            nn.Linear(4096, num_labels),
        )

    def freeze_initial(self):
        self.model.layer0.requires_grad_ = False
        self.model.layer1.requires_grad_ = False
        self.model.layer2.requires_grad_ = False
        self.model.layer3.requires_grad_ = False
        self.model.layer4.requires_grad_ = False

    def unfreeze_initial(self):
        self.model.layer0.requires_grad_ = False
        self.model.layer1.requires_grad_ = False
        self.model.layer2.requires_grad_ = False
        self.model.layer3.requires_grad_ = True
        self.model.layer4.requires_grad_ = True

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = (x - self.mean) / self.std
        features = self.features(x).mean(dim=(2, 3))
        return self.classifier(features)

    def get_features(self, x: torch.Tensor) -> torch.Tensor:
        x = (x - self.mean) / self.std
        features = self.features(x).mean(dim=(2, 3))
        return features

    @property
    def device(self) -> torch.device:
        return next(self.parameters()).device

    @torch.no_grad()
    def infer_on_sample(
        self, x: Union[List[np.ndarray], np.ndarray], max_input_size: int
    ) -> Tuple[np.ndarray, np.ndarray]:
        logits_list, features_list = [], []
        for sample in x:
            transforms = get_test_transforms(
                sample.shape[0], sample.shape[1], max_input_size
            )
            batch = transforms(sample)
            batch = (batch - self.mean) / self.std
            features = self.features(batch).mean(dim=(2, 3))
            logits = self.classifier(features).sigmoid()

            logits_list.append(copy2cpu(logits.mean(dim=0)))
            features_list.append(copy2cpu(features.mean(dim=0)))

        return np.stack(features_list, axis=0), np.stack(logits_list, axis=0)
