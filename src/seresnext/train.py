import shutil
import multiprocessing as mp
from pathlib import Path
from typing import Any, List

import click
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import tqdm
from functional import seq
from sklearn.metrics import f1_score
from src.seresnext.data import ImageDataset, get_dataloaders
from src.seresnext.model import Model
from src.common import Config, copy2cpu
from torch.utils.tensorboard.writer import SummaryWriter
import torchvision.utils

use_cuda = torch.cuda.is_available()


def save_models(output_path: Path, tensors: List[Any], names: List[str]):
    for tensor, name in zip(tensors, names):
        torch.save(tensor, (output_path / name).with_suffix(".pt"))


@click.command()
@click.argument("input_data_dir")
@click.argument("training_labels_file")
@click.argument("splits_dir")
@click.argument("output_model_dir")
def train(
    input_data_dir: str,
    training_labels_file: str,
    splits_dir: str,
    output_model_dir: str,
):
    config = Config["cnn_image"]
    training_config = config["train"]

    train_dataloader, valid_dataloader = get_dataloaders(
        input_data_dir,
        training_labels_file,
        splits_dir,
        batch_size=training_config["batch_size"],
        image_size=config["architecture"]["image_size"],
        num_workers=mp.cpu_count() - 1,
    )

    output_model_dir_path = Path(output_model_dir)
    output_model_dir_path.mkdir(exist_ok=True, parents=True)

    train_writer = SummaryWriter(output_model_dir_path / "train")
    valid_writer = SummaryWriter(output_model_dir_path / "valid")

    global_step = 0

    model = Model(
        config["architecture"]["model_name"],
        config["architecture"]["num_labels"],
    )
    if use_cuda:
        model = model.cuda()

    optimizer = optim.AdamW(model.parameters(), lr=1e-4)

    loss_func = nn.BCEWithLogitsLoss(reduction="none")
    device = torch.device("cuda")

    #
    scheduler = optim.lr_scheduler.CosineAnnealingLR(
        optimizer,
        T_max=5 * len(train_dataloader),
        eta_min=1e-6,
    )
    best_val_loss = torch.tensor(np.inf)
    worst_files_folder = output_model_dir_path / "worst_files"
    worst_files_folder.mkdir(exist_ok=True, parents=True)
    model.freeze_initial()

    for epoch in range(5):
        print("Epoch: {}/{}".format(epoch + 1, training_config["num_epochs"]))

        model.train()

        pbar = tqdm.tqdm(train_dataloader, total=len(train_dataloader))
        for train_batch in pbar:
            optimizer.zero_grad()
            images = train_batch["images"]
            labels = train_batch["labels"]

            if use_cuda:
                images = images.cuda()
                labels = labels.cuda()

            predictions = model(images)

            loss = loss_func(predictions, labels).mean()
            score = f1_score(
                copy2cpu(labels.view((-1,)).byte()),
                copy2cpu(
                    (
                        predictions.sigmoid().view((-1,))
                        >= config["architecture"]["prediction_threshold"]
                    ).byte()
                ),
                average="macro",
            )

            loss.backward()
            optimizer.step()
            scheduler.step()

            train_writer.add_scalar("loss", loss, global_step=global_step)
            train_writer.add_scalar(
                "lr", optimizer.param_groups[0]["lr"], global_step=global_step
            )
            train_writer.add_scalar("f1", score, global_step=global_step)

            if global_step % training_config["log_img_freq"] == 0:
                train_writer.add_image(
                    "input_images",
                    torchvision.utils.make_grid(images),
                    global_step=global_step,
                )

            pbar.set_postfix({"loss": "%.4f" % loss, "score": "%.4f" % score})
            global_step += 1

        model.eval()

        val_loss = torch.tensor(0.0).to(device)
        val_score = 0.0
        total_samples = 0

        per_sample_losses = []
        per_sample_indices = []

        with torch.no_grad():
            for valid_batch in tqdm.tqdm(valid_dataloader, leave=False):
                images = valid_batch["images"]
                labels = valid_batch["labels"]

                if use_cuda:
                    images = images.cuda()
                    labels = labels.cuda()

                predictions = model(images)

                loss = loss_func(predictions, labels)
                score = f1_score(
                    copy2cpu(labels.view((-1,)).byte()),
                    copy2cpu(
                        (
                            predictions.sigmoid().view((-1,))
                            >= config["architecture"]["prediction_threshold"]
                        ).byte()
                    ),
                    average="macro",
                )

                val_score += score * len(images)
                val_loss += loss.mean() * len(images)
                total_samples += len(images)

                per_sample_losses.append(loss.sum(dim=-1))
                per_sample_indices.append(valid_batch["indices"])

            valid_writer.add_scalar(
                "loss", val_loss / total_samples, global_step=global_step
            )

            valid_writer.add_scalar(
                "f1", val_score / total_samples, global_step=global_step
            )
            print("Val data:")
            print(
                {
                    "loss": "%.4f" % (val_loss / total_samples),
                    "score": "%.4f" % (val_score / total_samples),
                }
            )
        per_sample_losses_tensor = copy2cpu(torch.cat(per_sample_losses, dim=0))
        per_sample_indices_tensor = copy2cpu(
            torch.cat(per_sample_indices, dim=0)
        )

        dataset: ImageDataset = valid_dataloader.dataset
        files = dataset.get_file_names_by_indices(per_sample_indices_tensor)

        shutil.rmtree(worst_files_folder.as_posix())
        worst_files_folder.mkdir(exist_ok=True, parents=True)
        seq(zip(per_sample_losses_tensor, files)).sorted(
            lambda x: x[0], reverse=True
        ).take(training_config["top_worst_images_to_save"]).for_each(
            lambda x: shutil.copy2(
                x[1].as_posix(),
                (
                    (
                        worst_files_folder
                        / (
                            x[1].with_suffix("").name
                            + ("_{:.4f}.{}".format(x[0], x[1].suffix))
                        )
                    )
                ),
            )
        )

        if val_loss < best_val_loss:
            save_models(output_model_dir_path, [model.state_dict()], ["model"])
            best_val_loss = val_loss.item()

        pbar.close()

    # retrain
    model.unfreeze_initial()
    scheduler = optim.lr_scheduler.OneCycleLR(
        optimizer,
        total_steps=5 * len(train_dataloader),
        max_lr=1e-5,
    )

    for epoch in range(5):
        print("Epoch: {}/{}".format(epoch + 1, training_config["num_epochs"]))

        model.train()

        pbar = tqdm.tqdm(train_dataloader, total=len(train_dataloader))
        for train_batch in pbar:
            optimizer.zero_grad()
            images = train_batch["images"]
            labels = train_batch["labels"]

            if use_cuda:
                images = images.cuda()
                labels = labels.cuda()

            predictions = model(images)

            loss = loss_func(predictions, labels).mean()
            score = f1_score(
                copy2cpu(labels.view((-1,)).byte()),
                copy2cpu(
                    (
                        predictions.sigmoid().view((-1,))
                        >= config["architecture"]["prediction_threshold"]
                    ).byte()
                ),
                average="macro",
            )

            loss.backward()
            optimizer.step()
            scheduler.step()

            train_writer.add_scalar("loss", loss, global_step=global_step)
            train_writer.add_scalar(
                "lr", optimizer.param_groups[0]["lr"], global_step=global_step
            )
            train_writer.add_scalar("f1", score, global_step=global_step)

            if global_step % training_config["log_img_freq"] == 0:
                train_writer.add_image(
                    "input_images",
                    torchvision.utils.make_grid(images),
                    global_step=global_step,
                )

            pbar.set_postfix({"loss": "%.4f" % loss, "score": "%.4f" % score})
            global_step += 1

        model.eval()

        val_loss = torch.tensor(0.0).to(device)
        val_score = 0.0
        total_samples = 0

        per_sample_losses = []
        per_sample_indices = []

        with torch.no_grad():
            for valid_batch in tqdm.tqdm(valid_dataloader, leave=False):
                images = valid_batch["images"]
                labels = valid_batch["labels"]

                if use_cuda:
                    images = images.cuda()
                    labels = labels.cuda()

                predictions = model(images)

                loss = loss_func(predictions, labels)
                score = f1_score(
                    copy2cpu(labels.view((-1,)).byte()),
                    copy2cpu(
                        (
                            predictions.sigmoid().view((-1,))
                            >= config["architecture"]["prediction_threshold"]
                        ).byte()
                    ),
                    average="macro",
                )

                val_score += score * len(images)
                val_loss += loss.mean() * len(images)
                total_samples += len(images)

                per_sample_losses.append(loss.sum(dim=-1))
                per_sample_indices.append(valid_batch["indices"])

            valid_writer.add_scalar(
                "loss", val_loss / total_samples, global_step=global_step
            )

            valid_writer.add_scalar(
                "f1", val_score / total_samples, global_step=global_step
            )
            print("Val data:")
            print(
                {
                    "loss": "%.4f" % (val_loss / total_samples),
                    "score": "%.4f" % (val_score / total_samples),
                }
            )
        per_sample_losses_tensor = copy2cpu(
            torch.cat(per_sample_losses, dim=0)
        )
        per_sample_indices_tensor = copy2cpu(
            torch.cat(per_sample_indices, dim=0)
        )

        if val_loss < best_val_loss:
            save_models(
                output_model_dir_path,
                [
                    model.state_dict(),
                ],
                ["model"],
            )
            best_val_loss = val_loss.item()

        pbar.close()


if __name__ == "__main__":
    train()
