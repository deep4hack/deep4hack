from pathlib import Path
from typing import Dict, List, Tuple

import cv2
import imgaug.augmenters as iaa
import numpy as np
import pandas as pd
import PIL.Image
import torch
from functional import seq
from torch.utils.data.dataloader import DataLoader
from torch.utils.data.dataset import Dataset
from torchvision.transforms import (
    Compose,
    RandomCrop,
    Resize,
    ToPILImage,
    ToTensor,
)
from torchvision.transforms.transforms import Lambda, TenCrop


class RandomTransforms:
    def __init__(self) -> None:
        self.augments = iaa.Sequential(
            [
                iaa.Sometimes(0.5, iaa.JpegCompression(50, 80)),
                iaa.Fliplr(0.5),
                iaa.Sometimes(0.25, iaa.Add((-25, 25))),
                iaa.Sometimes(0.5, iaa.Cutout((1, 4))),
                iaa.Sometimes(
                    0.2, iaa.OneOf([iaa.GaussianBlur(), iaa.MotionBlur()])
                ),
                iaa.Sometimes(0.2, iaa.GammaContrast()),
                iaa.Sometimes(
                    0.2,
                    iaa.OneOf(
                        [
                            iaa.AdditiveGaussianNoise(),
                            iaa.AdditivePoissonNoise(),
                        ]
                    ),
                ),
                iaa.Sometimes(0.1, iaa.ChannelShuffle()),
            ],
            random_order=True,
        )

    def __call__(self, image: np.ndarray) -> np.ndarray:
        return self.augments.augment(image=image)


class ToNumpy:
    def __call__(self, x: PIL.Image) -> np.ndarray:
        return np.array(x)


def get_transforms(height: int, width: int, max_size: int) -> Compose:
    if height > width:
        new_width = max_size
        new_height = int(height * new_width / width)
    else:
        new_height = max_size
        new_width = int(width * new_height / height)

    return Compose(
        [
            ToPILImage(),
            Resize((new_height, new_width)),
            RandomCrop((max_size, max_size)),
            ToNumpy(),
            RandomTransforms(),
            ToTensor(),
        ]
    )


def get_test_transforms(height: int, width: int, max_size: int) -> Compose:
    if height > width:
        new_width = max_size
        new_height = int(height * new_width / width)
    else:
        new_height = max_size
        new_width = int(width * new_height / height)

    return Compose(
        [
            ToPILImage(),
            Resize((new_height, new_width)),
            TenCrop((max_size, max_size)),
            Lambda(
                lambda crops: torch.stack([ToTensor()(crop) for crop in crops])
            ),
        ]
    )


class ImageDataset(Dataset):
    def __init__(
        self,
        input_dir: str,
        training_labels_file: str,
        split_names: List[str],
        image_size: int,
        is_valid: bool,
        test_time_augmentation: bool = False,
        num_test_time_augmentations: int = 1,
    ) -> None:
        self.is_valid = is_valid
        split_names_set = set(split_names)

        self.files = np.array(
            (
                seq(Path(input_dir).rglob("*"))
                .filter(lambda x: x.name in split_names_set)
                .list()
            )
        )
        self.labels = pd.read_csv(training_labels_file)
        self.image_size = image_size
        self.test_time_augmentation = test_time_augmentation
        self.num_test_time_augmentations = num_test_time_augmentations

    def __len__(self) -> int:
        return len(self.files)

    def __getitem__(self, index: int) -> Dict[str, torch.Tensor]:
        file_path: Path = self.files[index]

        image = cv2.imread(file_path.as_posix())[..., ::-1]
        labels = torch.from_numpy(
            self.labels[self.labels["Name"] == file_path.name]
            .values[0, 1:]
            .astype(np.float32)
        )

        if self.test_time_augmentation:
            transforms = get_test_transforms(
                image.shape[0], image.shape[1], self.image_size
            )
            image_tensor = transforms(image)
            return {
                "images": image_tensor,
                "labels": labels,
                "indices": torch.tensor(index).int(),
                "names": file_path.name,
            }
        else:
            transforms = get_transforms(
                image.shape[0], image.shape[1], self.image_size
            )
            image_tensor = transforms(image)

            return {
                "images": image_tensor,
                "labels": labels,
                "indices": torch.tensor(index).int(),
                "names": file_path.name,
            }

    def get_file_names_by_indices(self, indices: np.ndarray) -> List[Path]:
        return self.files[indices]


class TestDataset(Dataset):
    def __init__(
        self,
        input_dir: str,
        image_size: int,
        test_time_augmentation: bool,
        num_test_time_augmentations: int,
    ) -> None:
        self.files = np.array((seq(Path(input_dir).rglob("*")).list()))
        self.test_time_augmentation = test_time_augmentation
        self.num_test_time_augmentations = num_test_time_augmentations
        self.image_size = image_size

    def __len__(self) -> int:
        return len(self.files)

    def __getitem__(self, index: int) -> Dict[str, torch.Tensor]:
        file_path: Path = self.files[index]

        image = cv2.imread(file_path.as_posix())[..., ::-1]

        transforms = get_test_transforms(
            image.shape[0], image.shape[1], self.image_size
        )
        image_tensor = transforms(image)

        return {"images": image_tensor, "names": file_path.name}


def get_dataloaders(
    input_data_dir: str,
    training_labels_file: str,
    splits_dir: str,
    batch_size: int,
    image_size: int,
    num_workers: int,
    test_time_augmentation: bool = False,
    num_test_time_augmentations: int = 1,
    no_shuffling: bool = False,
) -> Tuple[DataLoader, DataLoader]:
    train_split_names = (
        seq((Path(splits_dir) / "train.txt").read_text().split())
        .filter(lambda x: len(x) > 0)
        .list()
    )
    valid_split_names = (
        seq((Path(splits_dir) / "validation.txt").read_text().split())
        .filter(lambda x: len(x) > 0)
        .list()
    )
    train_dataset = ImageDataset(
        input_data_dir,
        training_labels_file,
        train_split_names,
        image_size,
        is_valid=False,
        test_time_augmentation=test_time_augmentation,
        num_test_time_augmentations=num_test_time_augmentations,
    )
    valid_dataset = ImageDataset(
        input_data_dir,
        training_labels_file,
        valid_split_names,
        image_size,
        is_valid=True,
        test_time_augmentation=test_time_augmentation,
        num_test_time_augmentations=num_test_time_augmentations,
    )

    train_loader = DataLoader(
        train_dataset,
        batch_size=batch_size,
        shuffle=not no_shuffling,
        drop_last=not no_shuffling,
        pin_memory=True,
        num_workers=num_workers,
    )

    valid_loader = DataLoader(
        valid_dataset,
        batch_size=batch_size,
        shuffle=False,
        drop_last=False,
        pin_memory=True,
        num_workers=num_workers,
    )
    return train_loader, valid_loader


def get_test_loader(
    input_data_dir: str,
    test_time_augmentation: bool,
    num_test_time_augmentations: int,
    batch_size: int,
    image_size: int,
    num_workers: int,
) -> DataLoader:
    test_dataset = TestDataset(
        input_data_dir,
        image_size,
        test_time_augmentation,
        num_test_time_augmentations,
    )

    test_loader = DataLoader(
        test_dataset,
        batch_size=batch_size,
        shuffle=False,
        drop_last=False,
        pin_memory=True,
        num_workers=num_workers,
    )

    return test_loader
