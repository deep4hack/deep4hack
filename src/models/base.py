"""Base model class implementation"""


class BaseModel(object):
    """"""

    def __init__(self):
        """Constructor."""
        pass

    def load(self, path):
        """Load model from file."""
        pass

    def dump(self, path):
        """Dump model to file."""
        pass

    def train(self):
        """Train model."""
        pass

    def predict(self, y):
        """Predict labels for y.

        Model should be loaded or trained first."""
        pass
