FROM nvidia/cuda:10.2-cudnn7-devel-ubuntu18.04

RUN apt update && \
    apt install -y \
        python3.8  \
        python3.8-dev \
        python3-pip \
        zsh \
        less \
	    git \
	    wget \
	    tmux \
	    htop \
	    vim \
	    gcc \
	    g++ \
	    software-properties-common \
	    ffmpeg \
        libsm6 \
        libxext6

RUN chsh -s /bin/zsh && \
    zsh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"

WORKDIR /home/deep4hack
COPY requirements.txt .

RUN python3.8 -m pip install --upgrade pip
RUN python3.8 -m pip install -r requirements.txt

RUN mkdir /.streamlit && chown -R 1000 /.streamlit

RUN python3.8 -m pip install pydub==0.24.1 gensim==3.8.3
RUN python3.8 -m spacy download pl_core_news_sm

RUN python3.8 -m pip install plotly==4.12.0 pyfunctional==1.4.2

ENTRYPOINT ["zsh"]
